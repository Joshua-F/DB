## DBRow 1733 ([**51**](../dbtable/51.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 1
| row_id | string     |
| ------ | ---------- |
| 0      | Storehouse |
### Table 3
| row_id | graphic |
| ------ | ------- |
| 0      | 894     |
| 1      | 895     |
| 2      | 896     |
### Table 4
| row_id | dbrow |
| ------ | ----- |
| 0      | 1727  |
### Table 5
| row_id | int   |
| ------ | ----- |
| 0      | 3000  |
| 1      | 22000 |
| 2      | 75000 |
### Table 6
| row_id | dbrow |
| ------ | ----- |
| 0      | 1729  |
### Table 7
| row_id | int   |
| ------ | ----- |
| 0      | 3000  |
| 1      | 22000 |
| 2      | 75000 |
### Table 8
| row_id | dbrow |
| ------ | ----- |
| 0      | 1730  |
### Table 9
| row_id | int   |
| ------ | ----- |
| 0      | 3000  |
| 1      | 22000 |
| 2      | 75000 |
### Table 10
| row_id | string                                                    |
| ------ | --------------------------------------------------------- |
| 0      | Maximum resources increased to 25,000 per resource type.  |
| 1      | Maximum resources increased to 80,000 per resource type.  |
| 2      | Maximum resources increased to 150,000 per resource type. |
### Table 11
| row_id | dbrow |
| ------ | ----- |
| 0      | 1748  |
