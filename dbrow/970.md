## DBRow 970 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 114 |
### Table 1
| row_id | string    |
| ------ | --------- |
| 0      | Whetstone |
### Table 2
| row_id | string     |
| ------ | ---------- |
| 0      | Whetstones |
### Table 3
| row_id | string                                                                 |
| ------ | ---------------------------------------------------------------------- |
| 0      | A device used to repair items which are in a degraded or broken state. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 31756   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 100 |
