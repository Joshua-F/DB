## DBRow 1473 ([**34**](../dbtable/34.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 94  |
### Table 1
| row_id | string        |
| ------ | ------------- |
| 0      | Red sandstone |
### Table 3
| row_id | obj   | int | int |
| ------ | ----- | --- | --- |
| 0      | 23194 | 81  | 100 |
