## DBRow 3018 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 66  |
### Table 2
| row_id | string                                                                                                                                                                    |
| ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | This stringed instrument, also known as a lyre, has turtle shell as part of its construction. Sea turtles have been spotted swimming off the coast by our archaeologists. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 51  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49765 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49766 |
