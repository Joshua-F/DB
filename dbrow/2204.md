## DBRow 2204 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                     |
| ------ | ---------------------------------------------------------------------------------------------------------- |
| 0      | Tap this button to deposit the tin and copper ore in your backpack into your <col=FFCB05>metal bank</col>. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 2424854   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 2424854   | -1  |
### Table 21
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 2424854   | -1  |
