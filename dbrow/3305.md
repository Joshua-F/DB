## DBRow 3305 ([**40**](../dbtable/40.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 17  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 30  |
### Table 3
| row_id | graphic | string                                                                                                                                | vorbis | int | boolean | boolean | boolean | boolean |
| ------ | ------- | ------------------------------------------------------------------------------------------------------------------------------------- | ------ | --- | ------- | ------- | ------- | ------- |
| 0      | 10308   | Stormguard was Armadyl's secret research, development and<br>weapons testing facility, within which the Godsword was forged.          | 47933  | 450 | 1       | 0       | 1       | 1       |
| 1      | 10308   | Its presence shrouded in secrecy, along with a cloaking storm cloud,<br>and forever moving across the skies to protect its existence. | 47934  | 550 | 0       | 1       | 1       | 1       |
| 2      | 10310   | Its many secrets lost to time, as the citadel was<br>sundered by the Forinthry explosion and its remnants set adrift.                 | 47935  | 500 | 1       | 0       | 1       | 1       |
| 3      | 10310   | Yet now, a chance to recover the ahead-of-their-time designs<br>of the aviansie alchemist and inventor 'Howl'.                        | 47936  | 500 | 0       | 1       | 1       | 1       |
