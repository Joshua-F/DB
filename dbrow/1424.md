## DBRow 1424 ([**34**](../dbtable/34.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 1
| row_id | string   |
| ------ | -------- |
| 0      | Tin rock |
### Table 3
| row_id | obj | int | int |
| ------ | --- | --- | --- |
| 0      | 438 | 1   | 100 |
