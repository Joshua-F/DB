## DBRow 1508 ([**38**](../dbtable/38.md))
### Table 0
| row_id | string |
| ------ | ------ |
| 0      | Dorbo  |
### Table 1
| row_id | string                                                                                                                                              |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Really good with young large animals and sleeping.<br>Babysitter farmhand - Prevents animals growing past the adolescent stage.<br>Large pens only. |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 3
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 13
| row_id | int |
| ------ | --- |
| 0      | 10  |
### Table 17
| row_id | dbrow |
| ------ | ----- |
| 0      | 327   |
| 1      | 328   |
### Table 18
| row_id | obj   |
| ------ | ----- |
| 0      | 40429 |
### Table 19
| row_id | int  |
| ------ | ---- |
| 0      | 2600 |
