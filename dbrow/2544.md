## DBRow 2544 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                                                                 |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | The top number is your modified skill level (temporarily boosted or drained by skill buffs and debuffs - such as potions or spells). It is used for most skill checks. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 96075776  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 96075780  | 2   |
