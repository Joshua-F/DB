## DBRow 2100 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                 |
| ------ | -------------------------------------------------------------------------------------- |
| 0      | Tap the <col=FFCB05>Track</col> button to track the <col=FFCB05>Daily Challenge</col>. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 88014900  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 88014900  | -1  |
### Table 21
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 88014900  | -1  |
### Table 23
| row_id | int |
| ------ | --- |
| 0      | 1   |
