## DBRow 2534 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                                                                  |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | To find the start of a quest, look for the <col=FFCB05>quest icon</col> on your <col=FFCB05>minimap</col>. You'll usually start the quest by talking to someone nearby. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 96010240  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 15
| row_id | graphic |
| ------ | ------- |
| 0      | 21017   |
