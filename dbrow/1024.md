## DBRow 1024 ([**7**](../dbtable/7.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 14  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | string                                 |
| ------ | -------------------------------------- |
| 0      | Upstairs in the East Ardougne windmill |
