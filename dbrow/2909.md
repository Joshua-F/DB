## DBRow 2909 ([**94**](../dbtable/94.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 18  |
### Table 1
| row_id | string         |
| ------ | -------------- |
| 0      | Inspire Effort |
### Table 2
| row_id | string                                                   |
| ------ | -------------------------------------------------------- |
| 0      | You will gain 2% more XP when training gathering skills. |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 250 |
### Table 6
| row_id | graphic |
| ------ | ------- |
| 0      | 10349   |
### Table 7
| row_id | graphic |
| ------ | ------- |
| 0      | 10380   |
### Table 8
| row_id | graphic |
| ------ | ------- |
| 0      | 10411   |
### Table 9
| row_id | obj   |
| ------ | ----- |
| 0      | 49606 |
