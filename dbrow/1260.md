## DBRow 1260 ([**29**](../dbtable/29.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 120 |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 120 |
### Table 5
| row_id | string         |
| ------ | -------------- |
| 0      | Araxyte spider |
### Table 6
| row_id | string                               |
| ------ | ------------------------------------ |
| 0      | Eight times the fun of most animals. |
### Table 7
| row_id | int  |
| ------ | ---- |
| 0      | 1512 |
### Table 8
| row_id | int |
| ------ | --- |
| 0      | 151 |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 378 |
### Table 10
| row_id | int |
| ------ | --- |
| 0      | 832 |
### Table 11
| row_id | int |
| ------ | --- |
| 0      | 107 |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 108 |
### Table 13
| row_id | int |
| ------ | --- |
| 0      | 800 |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 150 |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 17
| row_id | int |
| ------ | --- |
| 0      | 180 |
### Table 18
| row_id | int |
| ------ | --- |
| 0      | 500 |
### Table 19
| row_id | int |
| ------ | --- |
| 0      | 3   |
| 1      | 8   |
### Table 20
| row_id | int |
| ------ | --- |
| 0      | 60  |
### Table 21
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 24
| row_id | int   |
| ------ | ----- |
| 0      | 10000 |
### Table 25
| row_id | int   |
| ------ | ----- |
| 0      | 15000 |
### Table 26
| row_id | int   |
| ------ | ----- |
| 0      | 30000 |
### Table 27
| row_id | int   |
| ------ | ----- |
| 0      | 45000 |
### Table 28
| row_id | int |
| ------ | --- |
| 0      | 250 |
### Table 39
| row_id | struct |
| ------ | ------ |
| 0      | 41276  |
### Table 40
| row_id | struct |
| ------ | ------ |
| 0      | 41277  |
### Table 41
| row_id | struct |
| ------ | ------ |
| 0      | 41278  |
### Table 42
| row_id | enum  | enum  |
| ------ | ----- | ----- |
| 0      | 14326 | 14327 |
### Table 47
| row_id | obj   |
| ------ | ----- |
| 0      | 43677 |
### Table 48
| row_id | int |
| ------ | --- |
| 0      | 2   |
