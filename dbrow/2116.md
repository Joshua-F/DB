## DBRow 2116 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                      |
| ------ | ------------------------------------------- |
| 0      | Abilities can be accessed on an action-bar. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 126091286 |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
