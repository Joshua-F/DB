## DBRow 2769 ([**82**](../dbtable/82.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 1
| row_id | string             |
| ------ | ------------------ |
| 0      | Art Critic Jacques |
### Table 2
| row_id | npc  |
| ------ | ---- |
| 0      | 5930 |
### Table 4
| row_id | string                                     |
| ------ | ------------------------------------------ |
| 0      | Any painting you find, Jacques would like! |
### Table 5
| row_id | string         |
| ------ | -------------- |
| 0      | Varrock Museum |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 4   |
| 1      | 5   |
| 2      | 6   |
### Table 7
| row_id | dbrow |
| ------ | ----- |
| 0      | 2770  |
