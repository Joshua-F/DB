## DBRow 990 ([**22**](../dbtable/22.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 1
| row_id | string          |
| ------ | --------------- |
| 0      | Large generator |
### Table 2
| row_id | string                                                                        |
| ------ | ----------------------------------------------------------------------------- |
| 0      | Provides power to other machines. This generator will provide a lot of power. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 80  |
### Table 4
| row_id | int |
| ------ | --- |
| 0      | 122 |
### Table 5
| row_id | dbrow |
| ------ | ----- |
| 0      | 989   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 7
| row_id | dbrow | int |
| ------ | ----- | --- |
| 0      | 417   | 100 |
| 1      | 427   | 100 |
| 2      | 428   | 100 |
| 3      | 447   | 30  |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 20
| row_id | int |
| ------ | --- |
| 0      | 125 |
### Table 22
| row_id | int  |
| ------ | ---- |
| 0      | 5250 |
