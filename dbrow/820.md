## DBRow 820 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 71  |
### Table 1
| row_id | string    |
| ------ | --------- |
| 0      | Breakdown |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 28271   |
### Table 4
| row_id | string                                                                                                                                                          |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | When finishing a smithing item has a 0.8% chance per rank, of giving components as if you would've dissassembled the item. This does not dissassemble the item. |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 50  | 35  | 0   |
| 1      | 2   | 80  | 65  | 0   |
| 2      | 3   | 130 | 120 | 0   |
| 3      | 4   | 170 | 160 | 0   |
| 4      | 5   | 210 | 195 | 0   |
| 5      | 6   | 250 | 205 | 1   |
