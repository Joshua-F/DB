## DBRow 2650 ([**84**](../dbtable/84.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 65  |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 4
| row_id | string              |
| ------ | ------------------- |
| 0      | Oculi apoterrasaurs |
### Table 8
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 11
| row_id | obj   |
| ------ | ----- |
| 0      | 48864 |
| 1      | 48865 |
| 2      | 48866 |
| 3      | 48867 |
