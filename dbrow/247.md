## DBRow 247 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 41  |
### Table 1
| row_id | string               |
| ------ | -------------------- |
| 0      | Augmented leg armour |
### Table 3
| row_id | string                                    |
| ------ | ----------------------------------------- |
| 0      | Unlock the ability to augment leg armour. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 26248   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 45  |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 70  |
