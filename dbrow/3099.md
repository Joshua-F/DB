## DBRow 3099 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 147 |
### Table 2
| row_id | string                                                                                                                                                                           |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Finally, we have discovered an icon of the Empty Lord himself. No such icons have been discovered at the Varrock Dig Site, so we are lucky with how well preserved Kharid-et is. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 17  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49927 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49928 |
