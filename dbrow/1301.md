## DBRow 1301 ([**31**](../dbtable/31.md))
### Table 0
| row_id | string           |
| ------ | ---------------- |
| 0      | UI Customisation |
### Table 1
| row_id | string                                                       |
| ------ | ------------------------------------------------------------ |
| 0      | These settings allow further customisation to the interface. |
### Table 2
| row_id | struct |
| ------ | ------ |
| 0      | 41662  |
| 1      | 41785  |
| 2      | 41660  |
| 3      | 41659  |
| 4      | 41661  |
| 5      | 41663  |
| 6      | 41786  |
| 7      | 41740  |
| 8      | 4291   |
### Table 4
| row_id | int |
| ------ | --- |
| 0      | 1   |
