## DBRow 1862 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 143 |
### Table 1
| row_id | string                     |
| ------ | -------------------------- |
| 0      | Demon slayer ability codex |
### Table 3
| row_id | string                                                                  |
| ------ | ----------------------------------------------------------------------- |
| 0      | Unlocks an ability that increases damage against demons when activated. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 82      |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 88  |
