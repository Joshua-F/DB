## DBRow 2483 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                      |
| ------ | ----------------------------------------------------------- |
| 0      | These are some options you might want to adjust frequently. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 21233683  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 21
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 21233683  | -1  |
