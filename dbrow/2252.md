## DBRow 2252 ([**66**](../dbtable/66.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 18  |
### Table 1
| row_id | int  |
| ------ | ---- |
| 0      | 2000 |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 28254   |
### Table 4
| row_id | string         |
| ------ | -------------- |
| 0      | Fishing Tokens |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 1   |
