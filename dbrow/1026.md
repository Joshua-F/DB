## DBRow 1026 ([**7**](../dbtable/7.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 16  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | string                            |
| ------ | --------------------------------- |
| 0      | Next to the Taverley Stone Circle |
