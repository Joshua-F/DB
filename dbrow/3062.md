## DBRow 3062 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 110 |
### Table 2
| row_id | string                                                                                                                           |
| ------ | -------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Lions were understood to have been introduced to Gielinor by Saradomin, or at least most frequently fought alongside the icyene. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 81  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49853 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49854 |
