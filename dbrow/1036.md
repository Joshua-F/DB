## DBRow 1036 ([**7**](../dbtable/7.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 26  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | string                                                  |
| ------ | ------------------------------------------------------- |
| 0      | Near the Sawmill Operator's booth north east of Varrock |
