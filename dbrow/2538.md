## DBRow 2538 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                  |
| ------ | --------------------------------------------------------------------------------------- |
| 0      | The blue border on your minimap shows the area containing your current quest objective. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 96010240  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 15
| row_id | graphic |
| ------ | ------- |
| 0      | 18901   |
