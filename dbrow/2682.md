## DBRow 2682 ([**84**](../dbtable/84.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 124 |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 4
| row_id | string           |
| ------ | ---------------- |
| 0      | Saradominist III |
### Table 5
| row_id | boolean | boolean |
| ------ | ------- | ------- |
| 0      | 1       | 1       |
### Table 6
| row_id | string             |
| ------ | ------------------ |
| 0      | Tetracompass piece |
### Table 9
| row_id | struct |
| ------ | ------ |
| 0      | 12293  |
### Table 11
| row_id | obj   |
| ------ | ----- |
| 0      | 49752 |
| 1      | 49772 |
| 2      | 49744 |
| 3      | 49800 |
| 4      | 49786 |
| 5      | 49792 |
