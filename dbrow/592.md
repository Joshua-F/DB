## DBRow 592 ([**9**](../dbtable/9.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 21  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 2
| row_id | string  |
| ------ | ------- |
| 0      | Unknown |
### Table 3
| row_id | string                                                                                |
| ------ | ------------------------------------------------------------------------------------- |
| 0      | There is no case info today. Go and check with Meg in Player Owned Ports why that is. |
### Table 6
| row_id | graphic |
| ------ | ------- |
| 0      | 26709   |
### Table 7
| row_id | graphic |
| ------ | ------- |
| 0      | 26710   |
### Table 8
| row_id | graphic |
| ------ | ------- |
| 0      | 26711   |
### Table 9
| row_id | int  |
| ------ | ---- |
| 0      | 5199 |
### Table 11
| row_id | int |
| ------ | --- |
| 0      | 20  |
