## DBRow 89 ([**39**](../dbtable/39.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 6   |
### Table 6
| row_id | inv |
| ------ | --- |
| 0      | 851 |
### Table 7
| row_id | string               |
| ------ | -------------------- |
| 0      | southern pen (small) |
### Table 8
| row_id | string    |
| ------ | --------- |
| 0      | south pen |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 20  |
### Table 10
| row_id | int  |
| ------ | ---- |
| 0      | 6000 |
### Table 11
| row_id | obj  | int | boolean |
| ------ | ---- | --- | ------- |
| 0      | 8778 | 10  | 1       |
| 1      | 1539 | 75  | 1       |
### Table 13
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 44152 | 1   |
