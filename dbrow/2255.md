## DBRow 2255 ([**66**](../dbtable/66.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 55  |
### Table 1
| row_id | int   |
| ------ | ----- |
| 0      | 10000 |
### Table 3
| row_id | obj   |
| ------ | ----- |
| 0      | 39486 |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 7
| row_id | string   |
| ------ | -------- |
| 0      | Teleport |
### Table 8
| row_id | string  |
| ------ | ------- |
| 0      | Examine |
