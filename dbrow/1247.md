## DBRow 1247 ([**29**](../dbtable/29.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 4   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 90  |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 96  |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 100 |
### Table 4
| row_id | int |
| ------ | --- |
| 0      | 101 |
### Table 5
| row_id | string           |
| ------ | ---------------- |
| 0      | Summerdown sheep |
### Table 6
| row_id | string                                 |
| ------ | -------------------------------------- |
| 0      | Not a baaaad animal to have on a farm. |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 120 |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 24  |
### Table 10
| row_id | int |
| ------ | --- |
| 0      | 66  |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 86  |
### Table 13
| row_id | int    |
| ------ | ------ |
| 0      | 450000 |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 90  |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 17
| row_id | int |
| ------ | --- |
| 0      | 100 |
### Table 18
| row_id | int |
| ------ | --- |
| 0      | 450 |
### Table 19
| row_id | int |
| ------ | --- |
| 0      | 6   |
| 1      | 1   |
| 2      | 2   |
### Table 20
| row_id | int |
| ------ | --- |
| 0      | 60  |
### Table 21
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 25
| row_id | int  |
| ------ | ---- |
| 0      | 1250 |
### Table 26
| row_id | int  |
| ------ | ---- |
| 0      | 2189 |
### Table 27
| row_id | int  |
| ------ | ---- |
| 0      | 2813 |
### Table 28
| row_id | int |
| ------ | --- |
| 0      | 80  |
### Table 29
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 43633 | 10  |
| 1      | 43635 | 10  |
| 2      | 43634 | 10  |
### Table 30
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 31
| row_id | obj   |
| ------ | ----- |
| 0      | 43651 |
| 1      | 43653 |
| 2      | 43652 |
### Table 32
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 33
| row_id | obj   | obj   |
| ------ | ----- | ----- |
| 0      | 43631 | 43649 |
### Table 34
| row_id | obj   | obj   |
| ------ | ----- | ----- |
| 0      | 43632 | 43650 |
### Table 35
| row_id | obj   | obj   |
| ------ | ----- | ----- |
| 0      | 43635 | 43653 |
### Table 36
| row_id | obj   | obj   |
| ------ | ----- | ----- |
| 0      | 43636 | 43654 |
### Table 37
| row_id | obj   | obj   |
| ------ | ----- | ----- |
| 0      | 43634 | 43652 |
### Table 38
| row_id | obj   | obj   |
| ------ | ----- | ----- |
| 0      | 43633 | 43651 |
### Table 42
| row_id | enum  | enum  |
| ------ | ----- | ----- |
| 0      | 14324 | 14325 |
### Table 47
| row_id | obj   |
| ------ | ----- |
| 0      | 43654 |
### Table 48
| row_id | int |
| ------ | --- |
| 0      | 2   |
