## DBRow 1445 ([**34**](../dbtable/34.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 47  |
### Table 1
| row_id | string        |
| ------ | ------------- |
| 0      | Water channel |
### Table 3
| row_id | obj   | int | int |
| ------ | ----- | --- | --- |
| 0      | 44778 | 37  | 100 |
