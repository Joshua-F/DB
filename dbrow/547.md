## DBRow 547 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 53  |
### Table 1
| row_id | string    |
| ------ | --------- |
| 0      | Polishing |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 26435   |
### Table 4
| row_id | string                                                                                                                                                                |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Has a 3% chance per rank of transmuting a gathered resource to a higher tier.<br><col=0x7592A0>This perk has an increased chance to activate on level 20 items.</col> |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 50  | 35  | 0   |
| 1      | 2   | 100 | 80  | 0   |
| 2      | 3   | 200 | 150 | 0   |
| 3      | 4   | 240 | 160 | 1   |
