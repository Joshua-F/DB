## DBRow 2806 ([**86**](../dbtable/86.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | string   |
| ------ | -------- |
| 0      | Warforge |
### Table 3
| row_id | string                                                |
| ------ | ----------------------------------------------------- |
| 0      | An abandoned army buried, south-west of Feldip Hills. |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 76  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 8
| row_id | graphic |
| ------ | ------- |
| 0      | 10298   |
### Table 9
| row_id | struct |
| ------ | ------ |
| 0      | 32617  |
### Table 12
| row_id | npc   |
| ------ | ----- |
| 0      | 26997 |
### Table 13
| row_id | obj   |
| ------ | ----- |
| 0      | 49525 |
### Table 14
| row_id | enum  |
| ------ | ----- |
| 0      | 14067 |
### Table 15
| row_id | enum  |
| ------ | ----- |
| 0      | 14068 |
### Table 16
| row_id | dbrow |
| ------ | ----- |
| 0      | 3357  |
| 1      | 3358  |
| 2      | 3374  |
| 3      | 3362  |
| 4      | 3363  |
| 5      | 3367  |
| 6      | 3371  |
