## DBRow 986 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 131 |
### Table 1
| row_id | string                     |
| ------ | -------------------------- |
| 0      | Machine: Alchemiser mk. II |
### Table 3
| row_id | string                                                                                                         |
| ------ | -------------------------------------------------------------------------------------------------------------- |
| 0      | Converts any deposited item into gold! Mk. II requires no fire runes and has a higher capacity and throughput. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 31769   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 108 |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 130 |
