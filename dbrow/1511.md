## DBRow 1511 ([**38**](../dbtable/38.md))
### Table 0
| row_id | string |
| ------ | ------ |
| 0      | Susan  |
### Table 1
| row_id | string                                                                                           |
| ------ | ------------------------------------------------------------------------------------------------ |
| 0      | Babies are so cute!<br>Babysitter farmhand - Prevents animals growing past the adolescent stage. |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 8   |
### Table 3
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 13
| row_id | int |
| ------ | --- |
| 0      | 10  |
### Table 18
| row_id | obj   |
| ------ | ----- |
| 0      | 47343 |
### Table 19
| row_id | int  |
| ------ | ---- |
| 0      | 3000 |
