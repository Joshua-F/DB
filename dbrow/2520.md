## DBRow 2520 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                                                                      |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | You have earned your first level in a skill!<br><br>Higher skill levels unlock new activities: equipment to craft, locations to explore, quests to complete, and much more. |
### Table 4
| row_id | string           | int      |
| ------ | ---------------- | -------- |
| 0      | Congratulations! | 16763653 |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 96075776  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 3   |
