## DBRow 1423 ([**34**](../dbtable/34.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 1
| row_id | string      |
| ------ | ----------- |
| 0      | Copper rock |
### Table 3
| row_id | obj | int | int |
| ------ | --- | --- | --- |
| 0      | 436 | 1   | 100 |
