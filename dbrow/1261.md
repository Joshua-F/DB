## DBRow 1261 ([**29**](../dbtable/29.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 42  |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 49  |
### Table 5
| row_id | string          |
| ------ | --------------- |
| 0      | Grey chinchompa |
### Table 6
| row_id | string                          |
| ------ | ------------------------------- |
| 0      | Explosive fun for all the farm. |
### Table 7
| row_id | int  |
| ------ | ---- |
| 0      | 2520 |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 504 |
### Table 10
| row_id | int  |
| ------ | ---- |
| 0      | 1386 |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 41  |
### Table 13
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 160 |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 17
| row_id | int |
| ------ | --- |
| 0      | 150 |
### Table 18
| row_id | int |
| ------ | --- |
| 0      | 600 |
### Table 19
| row_id | int |
| ------ | --- |
| 0      | 7   |
### Table 20
| row_id | int |
| ------ | --- |
| 0      | 60  |
### Table 21
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 25
| row_id | int   |
| ------ | ----- |
| 0      | 30000 |
### Table 26
| row_id | int   |
| ------ | ----- |
| 0      | 52500 |
### Table 27
| row_id | int   |
| ------ | ----- |
| 0      | 67500 |
### Table 28
| row_id | int |
| ------ | --- |
| 0      | 250 |
### Table 39
| row_id | struct |
| ------ | ------ |
| 0      | 41273  |
### Table 40
| row_id | struct |
| ------ | ------ |
| 0      | 41274  |
### Table 41
| row_id | struct |
| ------ | ------ |
| 0      | 41275  |
### Table 42
| row_id | enum  | enum  |
| ------ | ----- | ----- |
| 0      | 14322 | 14323 |
### Table 47
| row_id | obj   |
| ------ | ----- |
| 0      | 43612 |
### Table 48
| row_id | int |
| ------ | --- |
| 0      | 1   |
