## DBRow 2956 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 7   |
### Table 2
| row_id | string                                                                                                                                                                                    |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Golem technology is a process mostly lost to those of us in the current age. It appears golem production was at its height during the Third Age, and at Stormguard Citadel especially so. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 98  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49642 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49643 |
