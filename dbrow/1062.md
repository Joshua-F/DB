## DBRow 1062 ([**7**](../dbtable/7.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 52  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 2
| row_id | string                                        |
| ------ | --------------------------------------------- |
| 0      | South west of the Charm Sprite cracked dolmen |
