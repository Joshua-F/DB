## DBRow 2301 ([**29**](../dbtable/29.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 11  |
### Table 1
| row_id | int  |
| ------ | ---- |
| 0      | 1102 |
### Table 3
| row_id | int  |
| ------ | ---- |
| 0      | 1106 |
### Table 5
| row_id | string            |
| ------ | ----------------- |
| 0      | Common green frog |
### Table 6
| row_id | string                        |
| ------ | ----------------------------- |
| 0      | He could croak at any minute. |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 720 |
### Table 8
| row_id | int |
| ------ | --- |
| 0      | 72  |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 180 |
### Table 10
| row_id | int |
| ------ | --- |
| 0      | 396 |
### Table 11
| row_id | int  |
| ------ | ---- |
| 0      | 1100 |
### Table 12
| row_id | int  |
| ------ | ---- |
| 0      | 1101 |
### Table 13
| row_id | int  |
| ------ | ---- |
| 0      | 5000 |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 17
| row_id | int |
| ------ | --- |
| 0      | 200 |
### Table 18
| row_id | int |
| ------ | --- |
| 0      | 600 |
### Table 19
| row_id | int |
| ------ | --- |
| 0      | 9   |
### Table 24
| row_id | int  |
| ------ | ---- |
| 0      | 5000 |
### Table 25
| row_id | int  |
| ------ | ---- |
| 0      | 7500 |
### Table 26
| row_id | int   |
| ------ | ----- |
| 0      | 15000 |
### Table 27
| row_id | int   |
| ------ | ----- |
| 0      | 22500 |
### Table 28
| row_id | int |
| ------ | --- |
| 0      | 108 |
### Table 29
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 48785 | 10  |
| 1      | 48786 | 10  |
| 2      | 48787 | 10  |
### Table 42
| row_id | enum  | enum  |
| ------ | ----- | ----- |
| 0      | 15787 | 15788 |
### Table 46
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 47
| row_id | obj   |
| ------ | ----- |
| 0      | 48784 |
### Table 48
| row_id | int |
| ------ | --- |
| 0      | 4   |
