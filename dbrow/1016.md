## DBRow 1016 ([**28**](../dbtable/28.md))
### Table 0
| row_id | string |
| ------ | ------ |
| 0      |        |
### Table 3
| row_id | string |
| ------ | ------ |
| 0      |        |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 7
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
