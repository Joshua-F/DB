## DBRow 2893 ([**94**](../dbtable/94.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 1
| row_id | string            |
| ------ | ----------------- |
| 0      | Divine Conversion |
### Table 2
| row_id | string                                                                               |
| ------ | ------------------------------------------------------------------------------------ |
| 0      | When converting memories at a rift, you will convert your entire backpack in one go. |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 100 |
### Table 6
| row_id | graphic |
| ------ | ------- |
| 0      | 10364   |
### Table 7
| row_id | graphic |
| ------ | ------- |
| 0      | 10395   |
### Table 8
| row_id | graphic |
| ------ | ------- |
| 0      | 10432   |
### Table 9
| row_id | obj   |
| ------ | ----- |
| 0      | 49590 |
