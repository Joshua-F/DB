## DBRow 2314 ([**29**](../dbtable/29.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 16  |
### Table 1
| row_id | int  |
| ------ | ---- |
| 0      | 1038 |
### Table 3
| row_id | int  |
| ------ | ---- |
| 0      | 1043 |
### Table 5
| row_id | string    |
| ------ | --------- |
| 0      | Scimitops |
### Table 6
| row_id | string                      |
| ------ | --------------------------- |
| 0      | Ignore its toxic behaviour. |
### Table 7
| row_id | int   |
| ------ | ----- |
| 0      | 11400 |
### Table 8
| row_id | int  |
| ------ | ---- |
| 0      | 1140 |
### Table 9
| row_id | int  |
| ------ | ---- |
| 0      | 2850 |
### Table 10
| row_id | int  |
| ------ | ---- |
| 0      | 6270 |
### Table 11
| row_id | int  |
| ------ | ---- |
| 0      | 1036 |
### Table 12
| row_id | int  |
| ------ | ---- |
| 0      | 1037 |
### Table 13
| row_id | int  |
| ------ | ---- |
| 0      | 5000 |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 17
| row_id | int  |
| ------ | ---- |
| 0      | 1100 |
### Table 18
| row_id | int |
| ------ | --- |
| 0      | 800 |
### Table 19
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 24
| row_id | int    |
| ------ | ------ |
| 0      | 120000 |
### Table 25
| row_id | int    |
| ------ | ------ |
| 0      | 180000 |
### Table 26
| row_id | int    |
| ------ | ------ |
| 0      | 360000 |
### Table 27
| row_id | int    |
| ------ | ------ |
| 0      | 540000 |
### Table 28
| row_id | int  |
| ------ | ---- |
| 0      | 2115 |
### Table 29
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 48825 | 10  |
| 1      | 48826 | 10  |
### Table 42
| row_id | enum  | enum  |
| ------ | ----- | ----- |
| 0      | 15789 | 15790 |
### Table 46
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 47
| row_id | obj   |
| ------ | ----- |
| 0      | 48824 |
### Table 48
| row_id | int |
| ------ | --- |
| 0      | 6   |
