## DBRow 1741 ([**51**](../dbtable/51.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 101 |
### Table 1
| row_id | string      |
| ------ | ----------- |
| 0      | Deposit box |
### Table 3
| row_id | graphic |
| ------ | ------- |
| 0      | 1031    |
### Table 4
| row_id | dbrow |
| ------ | ----- |
| 0      | 1726  |
### Table 5
| row_id | int   |
| ------ | ----- |
| 0      | 30000 |
### Table 6
| row_id | dbrow |
| ------ | ----- |
| 0      | 1730  |
### Table 7
| row_id | int   |
| ------ | ----- |
| 0      | 30000 |
### Table 10
| row_id | string                                 |
| ------ | -------------------------------------- |
| 0      | Access to a deposit box on Anachronia. |
### Table 11
| row_id | dbrow |
| ------ | ----- |
| 0      | 1754  |
### Table 12
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
