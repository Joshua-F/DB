## DBRow 3052 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 100 |
### Table 2
| row_id | string                                                                                                                                          |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | This painting is title the Lake of Fire, suggesting this may be an abstract image of lava. Is this a depiction of the Source or somewhere else? |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 65  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49833 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49834 |
