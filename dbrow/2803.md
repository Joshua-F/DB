## DBRow 2803 ([**86**](../dbtable/86.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | string    |
| ------ | --------- |
| 0      | Everlight |
### Table 3
| row_id | string                                                   |
| ------ | -------------------------------------------------------- |
| 0      | A long-lost lighthouse off Morytania's south-east coast. |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 42  |
### Table 8
| row_id | graphic |
| ------ | ------- |
| 0      | 10295   |
### Table 9
| row_id | struct |
| ------ | ------ |
| 0      | 30929  |
### Table 12
| row_id | npc   |
| ------ | ----- |
| 0      | 26939 |
### Table 13
| row_id | obj   |
| ------ | ----- |
| 0      | 49519 |
### Table 14
| row_id | enum  |
| ------ | ----- |
| 0      | 14061 |
### Table 15
| row_id | enum  |
| ------ | ----- |
| 0      | 14062 |
### Table 16
| row_id | dbrow |
| ------ | ----- |
| 0      | 3133  |
| 1      | 3136  |
| 2      | 3139  |
| 3      | 3143  |
| 4      | 3146  |
| 5      | 3149  |
