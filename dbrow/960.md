## DBRow 960 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 104 |
### Table 1
| row_id | string                |
| ------ | --------------------- |
| 0      | Spirit tree re-rooter |
### Table 2
| row_id | string                 |
| ------ | ---------------------- |
| 0      | Spirit tree re-rooters |
### Table 3
| row_id | string                                                                                                |
| ------ | ----------------------------------------------------------------------------------------------------- |
| 0      | This item looks like it will give access to the root of the Spirit Tree network. Contains 10 charges. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 31759   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 92  |
