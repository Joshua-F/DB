## DBRow 1756 ([**53**](../dbtable/53.md))
### Table 1
| row_id | string              |
| ------ | ------------------- |
| 0      | Arcane apoterrasaur |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 7
| row_id | obj |
| ------ | --- |
| 0      | 383 |
### Table 8
| row_id | int | int |
| ------ | --- | --- |
| 0      | 75  | 55  |
### Table 11
| row_id | obj   |
| ------ | ----- |
| 0      | 47948 |
