## DBRow 2898 ([**94**](../dbtable/94.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 8   |
### Table 1
| row_id | string       |
| ------ | ------------ |
| 0      | Abyssal Link |
### Table 2
| row_id | string                                                                             |
| ------ | ---------------------------------------------------------------------------------- |
| 0      | Teleport spells from the spellbook no longer require runes, but award no magic XP. |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 250 |
### Table 6
| row_id | graphic |
| ------ | ------- |
| 0      | 10346   |
### Table 7
| row_id | graphic |
| ------ | ------- |
| 0      | 10377   |
### Table 8
| row_id | graphic |
| ------ | ------- |
| 0      | 10408   |
### Table 9
| row_id | obj   |
| ------ | ----- |
| 0      | 49595 |
