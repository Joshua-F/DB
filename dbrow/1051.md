## DBRow 1051 ([**7**](../dbtable/7.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 41  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 2
| row_id | string                                        |
| ------ | --------------------------------------------- |
| 0      | Inside the entrance of Tai Bwo Wannai village |
