## DBRow 3443 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 75  |
### Table 1
| row_id | string  |
| ------ | ------- |
| 0      | Fortune |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 10768   |
### Table 4
| row_id | string                                                                                                                                              |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Has a 0.5% chance per rank on gathering skills and 0.1% chance per rank on production skills to double and bank what you have gathered or produced. |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 60  | 45  | 1   |
| 1      | 2   | 130 | 90  | 1   |
| 2      | 3   | 250 | 180 | 1   |
