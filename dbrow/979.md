## DBRow 979 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 124 |
### Table 1
| row_id | string                         |
| ------ | ------------------------------ |
| 0      | Machine: Automatic hide tanner |
### Table 3
| row_id | string                                |
| ------ | ------------------------------------- |
| 0      | Converts hides into workable leather. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 31762   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 93  |
