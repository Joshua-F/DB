## DBRow 3011 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 59  |
### Table 2
| row_id | string                                                                                                                                                                                                                 |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | This strange device, at least partly clockwork, has no immediately obvious mechanical use. It has stumped our researchers. It is, however, incredibly intricate and well beyond what was thought possible at the time. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 84  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49751 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49752 |
