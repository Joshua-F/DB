## DBRow 962 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 106 |
### Table 1
| row_id | string                  |
| ------ | ----------------------- |
| 0      | Teleportation compactor |
### Table 2
| row_id | string                   |
| ------ | ------------------------ |
| 0      | Teleportation compactors |
### Table 3
| row_id | string                                                                                                                                  |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | A device which is capable of enhancing and combining teleportation devices, destroying the original and adding charges into a new item. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 31757   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 110 |
