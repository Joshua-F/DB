## DBRow 475 ([**4**](../dbtable/4.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 137 |
### Table 1
| row_id | string             |
| ------ | ------------------ |
| 0      | Noxious components |
### Table 2
| row_id | string                                                               |
| ------ | -------------------------------------------------------------------- |
| 0      | Fortunately you're a professional, or handling this could be deadly. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 26526   |
### Table 5
| row_id | vorbis |
| ------ | ------ |
| 0      | 12116  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 120 |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 4   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 30  |
### Table 10
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 3   | 40  | 8   |
### Table 11
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 3   | 40  | 8   |
