## DBRow 228 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 23  |
### Table 1
| row_id | string             |
| ------ | ------------------ |
| 0      | Zamorak components |
### Table 3
| row_id | string                                           |
| ------ | ------------------------------------------------ |
| 0      | Allows you to use Zamorak components in a gizmo. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 26222   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 74  |
