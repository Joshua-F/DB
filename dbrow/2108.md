## DBRow 2108 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                   |
| ------ | ---------------------------------------- |
| 0      | As you attack, you will gain adrenaline. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 126025757 |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 126025757 | -1  |
### Table 21
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 126025757 | -1  |
