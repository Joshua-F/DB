## DBRow 1028 ([**7**](../dbtable/7.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 18  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | string                      |
| ------ | --------------------------- |
| 0      | Among the Catherby beehives |
