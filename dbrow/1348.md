## DBRow 1348 ([**13**](../dbtable/13.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 2
| row_id | string          |
| ------ | --------------- |
| 0      | 3rd-6th August: |
### Table 3
| row_id | string       |
| ------ | ------------ |
| 0      | Clue scrolls |
### Table 5
| row_id | int | string                                                                                                                                       |
| ------ | --- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | 1   | 1 free reroll per clue scroll per day.                                                                                                       |
| 1      | 1   | Gilly Willikers will give you a free clue scroll per day (limited by total level).                                                           |
| 2      | 0   |         - Easy: 37-1400 total levels                                                                                                         |
| 3      | 0   |         - Medium: 1401-2000 total levels                                                                                                     |
| 4      | 0   |         - Hard: 2001-2500 total levels                                                                                                       |
| 5      | 0   |         - Elite: 2501+ total levels                                                                                                          |
| 6      | 1   | Each completed Clue Scroll will earn a gift box containing a refreshing treat that also has a chance to contain some of the following items: |
| 7      | 0   |         - Slayer VIP Tickets                                                                                                                 |
| 8      | 0   |         - Silverhawk Down                                                                                                                    |
| 9      | 0   |         - Tight springs                                                                                                                      |
| 10     | 0   |         - Charms                                                                                                                             |
| 11     | 0   |         - xp Lamp/Star                                                                                                                       |
| 12     | 1   | Improved chance to get clue scrolls from drops and pickpocketing.                                                                            |
### Table 6
| row_id | int  |
| ------ | ---- |
| 0      | 6001 |
### Table 7
| row_id | int  |
| ------ | ---- |
| 0      | 6004 |
