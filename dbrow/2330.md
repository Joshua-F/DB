## DBRow 2330 ([**29**](../dbtable/29.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 21  |
### Table 1
| row_id | int  |
| ------ | ---- |
| 0      | 1079 |
### Table 3
| row_id | int  |
| ------ | ---- |
| 0      | 1083 |
### Table 5
| row_id | string                     |
| ------ | -------------------------- |
| 0      | Oculi apoterrasaur oceanum |
### Table 6
| row_id | string                      |
| ------ | --------------------------- |
| 0      | Ignore its toxic behaviour. |
### Table 7
| row_id | int   |
| ------ | ----- |
| 0      | 14400 |
### Table 8
| row_id | int  |
| ------ | ---- |
| 0      | 1440 |
### Table 9
| row_id | int  |
| ------ | ---- |
| 0      | 3600 |
### Table 10
| row_id | int  |
| ------ | ---- |
| 0      | 7920 |
### Table 11
| row_id | int  |
| ------ | ---- |
| 0      | 1076 |
### Table 12
| row_id | int  |
| ------ | ---- |
| 0      | 1077 |
### Table 13
| row_id | int  |
| ------ | ---- |
| 0      | 5000 |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 17
| row_id | int  |
| ------ | ---- |
| 0      | 1300 |
### Table 18
| row_id | int |
| ------ | --- |
| 0      | 800 |
### Table 19
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 24
| row_id | int    |
| ------ | ------ |
| 0      | 165000 |
### Table 25
| row_id | int    |
| ------ | ------ |
| 0      | 247500 |
### Table 26
| row_id | int    |
| ------ | ------ |
| 0      | 495000 |
### Table 27
| row_id | int    |
| ------ | ------ |
| 0      | 742500 |
### Table 28
| row_id | int  |
| ------ | ---- |
| 0      | 3015 |
### Table 29
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 48864 | 10  |
| 1      | 48866 | 10  |
### Table 42
| row_id | enum  | enum  |
| ------ | ----- | ----- |
| 0      | 15789 | 15790 |
### Table 46
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 47
| row_id | obj   |
| ------ | ----- |
| 0      | 48865 |
### Table 48
| row_id | int |
| ------ | --- |
| 0      | 6   |
