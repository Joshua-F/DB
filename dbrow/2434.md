## DBRow 2434 ([**77**](../dbtable/77.md))
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 2
| row_id | int | int | int  |
| ------ | --- | --- | ---- |
| 0      | 1   | 0   | 2018 |
### Table 3
| row_id | int | int | int  |
| ------ | --- | --- | ---- |
| 0      | 2   | 0   | 2018 |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 7   |
