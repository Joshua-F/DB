## DBRow 3370 ([**88**](../dbtable/88.md))
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | string              |
| ------ | ------------------- |
| 0      | Warforge scrap pile |
### Table 4
| row_id | int |
| ------ | --- |
| 0      | 104 |
### Table 12
| row_id | obj   |
| ------ | ----- |
| 0      | 49525 |
### Table 13
| row_id | obj   | int  |
| ------ | ----- | ---- |
| 0      | 49460 | 5000 |
| 1      | 49482 | 5000 |
### Table 14
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 50133 | 200 |
### Table 15
| row_id | dbrow | int |
| ------ | ----- | --- |
| 0      | 2987  | 10  |
| 1      | 2997  | 10  |
