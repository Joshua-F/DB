## DBRow 3088 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 136 |
### Table 2
| row_id | string                                                                                                                                                                                               |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | The hood of a set of praetorian robes. It's very heavy and well armoured, which is unusual for most modern magic users. They include a covering for the face, presumably for the sake of anonymity.  |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 114 |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49905 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49906 |
