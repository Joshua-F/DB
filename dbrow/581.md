## DBRow 581 ([**9**](../dbtable/9.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 10  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 4   |
### Table 2
| row_id | string                            |
| ------ | --------------------------------- |
| 0      | Robber from the Darkness - Part 3 |
### Table 3
| row_id | string                                                                               |
| ------ | ------------------------------------------------------------------------------------ |
| 0      | Another person has been robbed. The Varrock general store has had some items stolen. |
### Table 6
| row_id | graphic |
| ------ | ------- |
| 0      | 26709   |
### Table 7
| row_id | graphic |
| ------ | ------- |
| 0      | 26710   |
### Table 8
| row_id | graphic |
| ------ | ------- |
| 0      | 26711   |
### Table 9
| row_id | int  |
| ------ | ---- |
| 0      | 5188 |
### Table 11
| row_id | int |
| ------ | --- |
| 0      | 5   |
