## DBRow 416 ([**4**](../dbtable/4.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 1
| row_id | string       |
| ------ | ------------ |
| 0      | Simple parts |
### Table 2
| row_id | string                           |
| ------ | -------------------------------- |
| 0      | Useful things for building with. |
### Table 3
| row_id | string                                                                |
| ------ | --------------------------------------------------------------------- |
| 0      | Ores, logs, hides, bars, planks, leather, basic weapons and clothing. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 26311   |
### Table 5
| row_id | vorbis |
| ------ | ------ |
| 0      | 11968  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 90  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 10
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 13  | 5   | 15  |
| 1      | 16  | 8   | 32  |
| 2      | 22  | 7   | 28  |
| 3      | 23  | 7   | 27  |
### Table 11
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 13  | 5   | 15  |
| 1      | 16  | 8   | 32  |
| 2      | 22  | 7   | 28  |
| 3      | 23  | 7   | 27  |
| 4      | 31  | 8   | 33  |
### Table 12
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 16  | 8   | 32  |
| 1      | 22  | 7   | 28  |
| 2      | 23  | 7   | 27  |
