## DBRow 3032 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 80  |
### Table 2
| row_id | string                                                                                                                                                                                  |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | This painting appears to depict a much-loved Dominion Games medallist, but the scene itself is quite melancholy, perhaps in an attempt to convey humility or hubris. It's hard to tell. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 105 |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49793 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49794 |
