## DBRow 2988 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 39  |
### Table 2
| row_id | string                                                                                                                                                                                                |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Given where these were found, the team can only speculate that these rings were placed over the horn of a nosorog. They come in different sizes and colours that seem to stack in a pleasing pattern. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 94  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49706 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49707 |
