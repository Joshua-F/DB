## DBRow 457 ([**4**](../dbtable/4.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 120 |
### Table 1
| row_id | string            |
| ------ | ----------------- |
| 0      | Direct components |
### Table 2
| row_id | string                                           |
| ------ | ------------------------------------------------ |
| 0      | These seem to have straightforward applications. |
### Table 3
| row_id | string                        |
| ------ | ----------------------------- |
| 0      | Spears, battleaxes, pickaxes. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 26509   |
### Table 5
| row_id | vorbis |
| ------ | ------ |
| 0      | 12072  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 90  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 10
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 21  | 12  | 44  |
| 1      | 3   | 9   | 33  |
| 2      | 1   | 12  | 45  |
### Table 11
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 21  | 12  | 44  |
| 1      | 3   | 9   | 33  |
### Table 12
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 21  | 12  | 44  |
| 1      | 58  | 12  | 40  |
| 2      | 70  | 9   | 32  |
