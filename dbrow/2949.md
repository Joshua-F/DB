## DBRow 2949 ([**91**](../dbtable/91.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 19  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 2
| row_id | string |
| ------ | ------ |
| 0      | Zanik  |
### Table 3
| row_id | string                                                                |
| ------ | --------------------------------------------------------------------- |
| 0      | Expert on Bandosian culture, digging tunnels and working underground. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 10455   |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 10490   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 8   |
### Table 8
| row_id | int |
| ------ | --- |
| 0      | 7   |
