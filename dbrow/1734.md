## DBRow 1734 ([**51**](../dbtable/51.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 1
| row_id | string            |
| ------ | ----------------- |
| 0      | Sleeping Quarters |
### Table 3
| row_id | graphic |
| ------ | ------- |
| 0      | 891     |
| 1      | 892     |
| 2      | 893     |
### Table 4
| row_id | dbrow |
| ------ | ----- |
| 0      | 1726  |
### Table 5
| row_id | int    |
| ------ | ------ |
| 0      | 10000  |
| 1      | 65000  |
| 2      | 130000 |
### Table 6
| row_id | dbrow |
| ------ | ----- |
| 0      | 1728  |
### Table 7
| row_id | int    |
| ------ | ------ |
| 0      | 10000  |
| 1      | 65000  |
| 2      | 130000 |
### Table 8
| row_id | dbrow |
| ------ | ----- |
| 0      | 1729  |
### Table 9
| row_id | int    |
| ------ | ------ |
| 0      | 10000  |
| 1      | 65000  |
| 2      | 130000 |
### Table 10
| row_id | string                                           |
| ------ | ------------------------------------------------ |
| 0      | 15 workers can be allocated to gather resources. |
| 1      | 30 workers can be allocated to gather resources. |
| 2      | 60 workers can be allocated to gather resources. |
### Table 11
| row_id | dbrow |
| ------ | ----- |
| 0      | 1749  |
