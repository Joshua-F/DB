## DBRow 2271 ([**66**](../dbtable/66.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 23  |
### Table 1
| row_id | int  |
| ------ | ---- |
| 0      | 8000 |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 14379   |
### Table 4
| row_id | string                                        |
| ------ | --------------------------------------------- |
| 0      | Mage Training Arena - Alchemist Pizazz points |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 2   |
