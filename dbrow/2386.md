## DBRow 2386 ([**74**](../dbtable/74.md))
### Table 0
| row_id | string     |
| ------ | ---------- |
| 0      | Fowl Fight |
### Table 5
| row_id | boolean |
| ------ | ------- |
| 0      | 0       |
### Table 9
| row_id | int | int | int | obj   | npc   | loc    | boolean | string      |
| ------ | --- | --- | --- | ----- | ----- | ------ | ------- | ----------- |
| 0      | 1   | 255 | 0   | 49137 | 26749 | 114669 | 1       | Kill a duck |
### Table 10
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
