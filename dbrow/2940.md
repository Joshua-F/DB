## DBRow 2940 ([**91**](../dbtable/91.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 10  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 2
| row_id | string |
| ------ | ------ |
| 0      | Easty  |
### Table 3
| row_id | string                                                         |
| ------ | -------------------------------------------------------------- |
| 0      | Caretaker of the mysterious monolith and safekeeper of relics. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 10446   |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 10476   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 4   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 6   |
