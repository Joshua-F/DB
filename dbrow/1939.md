## DBRow 1939 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                                                                                                                                                                                                                                  |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Each ability has a cooldown. This is a short period after using an ability when you can't use it again.<br>The cooldown for each ability can be found at the top right of the ability's tooltip.<br>When an ability's cooldown timer is in effect, a shading over the ability icon will slowly unwind as the cooldown time counts down. |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 15
| row_id | graphic |
| ------ | ------- |
| 0      | 14885   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 93716565  | -1  |
| 1      | 126091284 | -1  |
