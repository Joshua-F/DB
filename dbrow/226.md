## DBRow 226 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 21  |
### Table 1
| row_id | string               |
| ------ | -------------------- |
| 0      | Corporeal components |
### Table 3
| row_id | string                                             |
| ------ | -------------------------------------------------- |
| 0      | Allows you to use corporeal components in a gizmo. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 26230   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 40  |
### Table 13
| row_id | int | int |
| ------ | --- | --- |
| 0      | 1   | 0   |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 10  |
