## DBRow 2782 ([**83**](../dbtable/83.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 107 |
### Table 2
| row_id | string                 |
| ------ | ---------------------- |
| 0      | Everlight White Knight |
### Table 6
| row_id | dbrow |
| ------ | ----- |
| 0      | 2680  |
| 1      | 2681  |
| 2      | 2682  |
| 3      | 2683  |
