## DBRow 3379 ([**92**](../dbtable/92.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 30  |
### Table 1
| row_id | string             |
| ------ | ------------------ |
| 0      | You Have Chosen... |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 3
| row_id | dbrow |
| ------ | ----- |
| 0      | 2806  |
### Table 5
| row_id | struct |
| ------ | ------ |
| 0      | 26418  |
