## DBRow 2253 ([**66**](../dbtable/66.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 13  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 50  |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 28259   |
### Table 4
| row_id | string         |
| ------ | -------------- |
| 0      | Penguin Points |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 1   |
