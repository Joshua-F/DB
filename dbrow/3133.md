## DBRow 3133 ([**86**](../dbtable/86.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 201 |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 2
| row_id | string                                 |
| ------ | -------------------------------------- |
| 0      | Everlight - Mass grave excavation site |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 42  |
### Table 17
| row_id | dbrow |
| ------ | ----- |
| 0      | 3134  |
| 1      | 3135  |
| 2      | 2874  |
