## DBRow 2669 ([**84**](../dbtable/84.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 110 |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 4
| row_id | string             |
| ------ | ------------------ |
| 0      | Red Rum Relics III |
### Table 5
| row_id | boolean | boolean |
| ------ | ------- | ------- |
| 0      | 1       | 1       |
### Table 6
| row_id | string                                    |
| ------ | ----------------------------------------- |
| 0      | Helm of Terror (inside) (relic component) |
### Table 7
| row_id | string             |
| ------ | ------------------ |
| 0      | Tetracompass piece |
### Table 9
| row_id | struct |
| ------ | ------ |
| 0      | 12280  |
### Table 11
| row_id | obj   |
| ------ | ----- |
| 0      | 49709 |
| 1      | 49711 |
| 2      | 49713 |
| 3      | 49727 |
