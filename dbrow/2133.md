## DBRow 2133 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                |
| ------ | ------------------------------------------------------------------------------------- |
| 0      | Your basic abilities are currently activated by a special non-interactive action-bar. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 126156804 |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 10
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 21
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 126156804 | -1  |
