## DBRow 519 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 44  |
### Table 1
| row_id | string     |
| ------ | ---------- |
| 0      | Energising |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 26403   |
### Table 4
| row_id | string                                                                                                |
| ------ | ----------------------------------------------------------------------------------------------------- |
| 0      | Slice, Piercing Shot and Wrack deal 20% less damage, but generate 0.6 additional adrenaline per rank. |
### Table 5
| row_id | string                                                                                                                    |
| ------ | ------------------------------------------------------------------------------------------------------------------------- |
| 0      | This ability deals <col=ffffff>20%</col> less damage, but generates <col=ffffff>0.6</col> additional adrenaline per rank. |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 50  | 35  | 0   |
| 1      | 2   | 100 | 80  | 0   |
| 2      | 3   | 200 | 150 | 0   |
| 3      | 4   | 240 | 160 | 1   |
