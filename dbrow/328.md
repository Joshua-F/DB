## DBRow 328 ([**39**](../dbtable/39.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 6   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 6
| row_id | inv |
| ------ | --- |
| 0      | 856 |
### Table 7
| row_id | string                   |
| ------ | ------------------------ |
| 0      | southeastern pen (large) |
### Table 8
| row_id | string         |
| ------ | -------------- |
| 0      | south-east pen |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 60  |
### Table 10
| row_id | int   |
| ------ | ----- |
| 0      | 28000 |
### Table 11
| row_id | obj  | int | boolean |
| ------ | ---- | --- | ------- |
| 0      | 8782 | 20  | 1       |
| 1      | 4824 | 225 | 1       |
### Table 13
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 44154 | 1   |
