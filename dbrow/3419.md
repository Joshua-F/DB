## DBRow 3419 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 151 |
### Table 1
| row_id | string            |
| ------ | ----------------- |
| 0      | Augmented mattock |
### Table 2
| row_id | string             |
| ------ | ------------------ |
| 0      | Augmented mattocks |
### Table 3
| row_id | string                                                                                |
| ------ | ------------------------------------------------------------------------------------- |
| 0      | Allows you to use an augmentor on high-level mattocks to create an augmented version. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 26251   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 22  |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 16
| row_id | int  |
| ------ | ---- |
| 0      | 8000 |
