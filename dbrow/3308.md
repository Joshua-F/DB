## DBRow 3308 ([**86**](../dbtable/86.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 402 |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 2
| row_id | string                                            |
| ------ | ------------------------------------------------- |
| 0      | Stormguard Citadel - Welcome area excavation site |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 70  |
### Table 17
| row_id | dbrow |
| ------ | ----- |
| 0      | 3307  |
