## DBRow 1201 ([**28**](../dbtable/28.md))
### Table 0
| row_id | string          |
| ------ | --------------- |
| 0      | Aura Management |
### Table 1
| row_id | graphic |
| ------ | ------- |
| 0      | 21006   |
### Table 3
| row_id | string                      |
| ------ | --------------------------- |
| 0      | View and manage your auras. |
### Table 4
| row_id | boolean | string          |
| ------ | ------- | --------------- |
| 0      | 1       | Aura Management |
