## DBRow 1456 ([**34**](../dbtable/34.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 70  |
### Table 1
| row_id | string           |
| ------ | ---------------- |
| 0      | Orichalcite rock |
### Table 3
| row_id | obj   | int | int |
| ------ | ----- | --- | --- |
| 0      | 44822 | 60  | 100 |
