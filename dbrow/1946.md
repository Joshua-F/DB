## DBRow 1946 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                                                                                                                                                   |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Abilities in the gold rectangle on your action bar will automatically be activated from left to right. During combat you can click an ability to queue it, and it will be triggered in the next rotation.<br>Press OK when you are ready to be attacked. |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 93716552  | -1  |
| 1      | 93716565  | -1  |
| 2      | 93716578  | -1  |
