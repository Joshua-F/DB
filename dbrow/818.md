## DBRow 818 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 69  |
### Table 1
| row_id | string |
| ------ | ------ |
| 0      | Tinker |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 28225   |
### Table 4
| row_id | string                                                                                                                                                                                                                                         |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Has a 5% chance per rank to carry out higher quality work, awarding an extra 25% XP. While smithing, has a 2% chance per rank to double your progress.<br><col=0x7592A0>This perk has an increased chance to activate on level 20 items.</col> |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 40  | 20  | 0   |
| 1      | 2   | 70  | 30  | 0   |
| 2      | 3   | 150 | 40  | 0   |
| 3      | 4   | 180 | 50  | 1   |
