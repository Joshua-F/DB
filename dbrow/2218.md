## DBRow 2218 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                            |
| ------ | --------------------------------- |
| 0      | Select the item you want to sell. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 82903060  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 21
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 82903060  | -1  |
