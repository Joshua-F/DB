## DBRow 2934 ([**91**](../dbtable/91.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 2
| row_id | string   |
| ------ | -------- |
| 0      | Katarina |
### Table 3
| row_id | string                                                                                |
| ------ | ------------------------------------------------------------------------------------- |
| 0      | A recent graduate with a burgeoning specialisation in effigies, sculptures and icons. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 10439   |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 10469   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 0   |
