## DBRow 3338 ([**90**](../dbtable/90.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 30  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 3
| row_id | string      |
| ------ | ----------- |
| 0      | Howling Man |
### Table 4
| row_id | int |
| ------ | --- |
| 0      | 118 |
### Table 6
| row_id | string                                                                                                                                                               |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | An alchemical laboratory sits floating higher than other remaining sections of Stormguard, filled with all kinds of arcane and mystical devices we don't understand. |
### Table 7
| row_id | string                                                                                                                                                                                                                                                                                         |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | According to various blueprints and notebooks, this laboratory was home to an aviansie alchemist who went by the codename 'Howl' (likely to protect his true identity). His works are far ahead of other technology we have dug up from the Third Age, and his ideas well ahead of their time. |
### Table 8
| row_id | int  |
| ------ | ---- |
| 0      | 4000 |
### Table 15
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 49638 | 1   |
