## DBRow 2981 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 32  |
### Table 2
| row_id | string                                                                                                                                                                                           |
| ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 0      | We are unsure if this anchor was a wielded weapon or simply used in the typical way. Staining around the walls of the Crucible suggest it may have been purposefully flooded for special events. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 97  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49692 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49693 |
