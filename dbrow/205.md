## DBRow 205 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 1
| row_id | string      |
| ------ | ----------- |
| 0      | Charge pack |
### Table 2
| row_id | string       |
| ------ | ------------ |
| 0      | Charge packs |
### Table 3
| row_id | string                                                                    |
| ------ | ------------------------------------------------------------------------- |
| 0      | Added to your toolbelt, provides power to all equipped Invention devices. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 26245   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 16
| row_id | int  |
| ------ | ---- |
| 0      | 2000 |
