## DBRow 1315 ([**31**](../dbtable/31.md))
### Table 0
| row_id | string     |
| ------ | ---------- |
| 0      | Accept Aid |
### Table 1
| row_id | string                                                               |
| ------ | -------------------------------------------------------------------- |
| 0      | Decide which players can offer to aid or assist you in various ways. |
### Table 2
| row_id | struct |
| ------ | ------ |
| 0      | 41710  |
| 1      | 41709  |
| 2      | 41820  |
| 3      | 41706  |
| 4      | 41707  |
| 5      | 41708  |
| 6      | 41821  |
| 7      | 41711  |
| 8      | 41712  |
