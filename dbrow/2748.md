## DBRow 2748 ([**84**](../dbtable/84.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 31  |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 4
| row_id | string          |
| ------ | --------------- |
| 0      | The Shadow Reef |
### Table 8
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 9
| row_id | struct |
| ------ | ------ |
| 0      | 4498   |
### Table 11
| row_id | obj   |
| ------ | ----- |
| 0      | 47490 |
| 1      | 47494 |
| 2      | 47492 |
| 3      | 47506 |
| 4      | 47468 |
| 5      | 47469 |
| 6      | 47537 |
