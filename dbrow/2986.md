## DBRow 2986 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 37  |
### Table 2
| row_id | string                                                                                                                                                                                      |
| ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Zanik tells us the Horogothgar were infamous for eating their defeated enemies, including other goblins. Tellingly, the inside of this pot still holds an unpleasant lingering smell to it. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 119 |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49702 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49703 |
