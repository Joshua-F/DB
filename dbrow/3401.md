## DBRow 3401 ([**86**](../dbtable/86.md))
### Table 0
| row_id | int   |
| ------ | ----- |
| 0      | 10015 |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 2
| row_id | string                                                     |
| ------ | ---------------------------------------------------------- |
| 0      | God Wars Dungeon - Zamorak's Fortress east excavation site |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 29  |
### Table 17
| row_id | dbrow |
| ------ | ----- |
| 0      | 2881  |
