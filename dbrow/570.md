## DBRow 570 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 63  |
### Table 1
| row_id | string       |
| ------ | ------------ |
| 0      | Planted Feet |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 26461   |
### Table 4
| row_id | string                                                                                                                                        |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | The duration of Sunshine and Death's Swiftness is increased by <col=ffffff>25%</col>, but they no longer deal periodic damage to your target. |
### Table 5
| row_id | string                                                                                                                     |
| ------ | -------------------------------------------------------------------------------------------------------------------------- |
| 0      | The duration of this ability is increased by <col=ffffff>25%</col>, but it no longer deals periodic damage to your target. |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 60  | 60  | 0   |
