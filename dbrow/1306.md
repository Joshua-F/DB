## DBRow 1306 ([**31**](../dbtable/31.md))
### Table 0
| row_id | string      |
| ------ | ----------- |
| 0      | Combat Mode |
### Table 1
| row_id | string                                |
| ------ | ------------------------------------- |
| 0      | Change which style of combat you use. |
### Table 2
| row_id | struct |
| ------ | ------ |
| 0      | 41598  |
| 1      | 41599  |
| 2      | 41600  |
| 3      | 41738  |
| 4      | 41795  |
| 5      | 41505  |
| 6      | 41796  |
| 7      | 41506  |
| 8      | 41507  |
| 9      | 41508  |
