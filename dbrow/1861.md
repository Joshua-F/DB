## DBRow 1861 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 142 |
### Table 1
| row_id | string                      |
| ------ | --------------------------- |
| 0      | Undead slayer ability codex |
### Table 3
| row_id | string                                                                  |
| ------ | ----------------------------------------------------------------------- |
| 0      | Unlocks an ability that increases damage against undead when activated. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 83      |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 88  |
