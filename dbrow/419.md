## DBRow 419 ([**4**](../dbtable/4.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 1
| row_id | string      |
| ------ | ----------- |
| 0      | Magic parts |
### Table 2
| row_id | string                          |
| ------ | ------------------------------- |
| 0      | Materials with magical potency. |
### Table 3
| row_id | string                                                  |
| ------ | ------------------------------------------------------- |
| 0      | Runes, signs and portents, magic weapons, magic armour. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 26485   |
### Table 5
| row_id | vorbis |
| ------ | ------ |
| 0      | 11972  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 90  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 10
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 16  | 8   | 32  |
| 1      | 10  | 8   | 33  |
| 2      | 35  | 9   | 33  |
| 3      | 42  | 5   | 15  |
| 4      | 44  | 7   | 27  |
| 5      | 40  | 5   | 13  |
### Table 11
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 16  | 8   | 32  |
| 1      | 10  | 8   | 33  |
| 2      | 35  | 9   | 33  |
| 3      | 42  | 5   | 15  |
| 4      | 44  | 7   | 27  |
### Table 12
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 16  | 8   | 32  |
| 1      | 5   | 7   | 27  |
