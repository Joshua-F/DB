## DBRow 421 ([**4**](../dbtable/4.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 6   |
### Table 1
| row_id | string        |
| ------ | ------------- |
| 0      | Organic parts |
### Table 2
| row_id | string                                 |
| ------ | -------------------------------------- |
| 0      | Bits of something that was once alive. |
### Table 3
| row_id | string                                     |
| ------ | ------------------------------------------ |
| 0      | Seeds, bones, ashes, food, herbs, potions. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 26484   |
### Table 5
| row_id | vorbis |
| ------ | ------ |
| 0      | 11973  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 90  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 10
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 23  | 7   | 27  |
| 1      | 37  | 5   | 13  |
| 2      | 2   | 8   | 33  |
| 3      | 50  | 8   | 32  |
### Table 11
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 23  | 7   | 27  |
| 1      | 37  | 5   | 13  |
| 2      | 26  | 5   | 15  |
### Table 12
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 23  | 7   | 27  |
| 1      | 70  | 7   | 27  |
