## DBRow 3071 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 119 |
### Table 2
| row_id | string                                                                                                                           |
| ------ | -------------------------------------------------------------------------------------------------------------------------------- |
| 0      | An early form of watch, more akin to a portable sundial. Something like this would still have been quite effective and accurate. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 47  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49871 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49872 |
