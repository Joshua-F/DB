## DBRow 1913 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                                                                                                   |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | In the <col=FFCB05>Combat</col> spells section of your magic book, hover your mouse over <col=FFCB05>Water Strike</col>. The tooltip shows the name of the spell and underneath the type of spell it is. |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
