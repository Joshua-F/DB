## DBRow 1351 ([**13**](../dbtable/13.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 6   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 28  |
### Table 2
| row_id | string               |
| ------ | -------------------- |
| 0      | 28th - 31st December |
### Table 3
| row_id | string    |
| ------ | --------- |
| 0      | Minigames |
### Table 5
| row_id | int | string                                                                                                                                   |
| ------ | --- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | 1   | <col=FF981F>Barbarian Assault</col> - Double Bonus xp and gamble rewards.                                                                |
| 1      | 0   |                                                                                                                                          |
| 2      | 1   | <col=FF981F>Cabbage Facepunch Bonanza</col> - Extra 600 daily bonus points.                                                              |
| 3      | 0   |                                                                                                                                          |
| 4      | 1   | <col=FF981F>Castle Wars</col> - +1 gold ticket per game.                                                                                 |
| 5      | 0   |                                                                                                                                          |
| 6      | 1   | <col=FF981F>Dominion Tower</col> - Bosses killed count as two for the purposes of reward unlocks and earn 50% increased Dominion Factor. |
| 7      | 0   |                                                                                                                                          |
| 8      | 1   | <col=FF981F>Livid Farm</col> - 50% increased produce points.                                                                             |
| 9      | 0   |                                                                                                                                          |
| 10     | 1   | <col=FF981F>Pest Control</col> - Double points.                                                                                          |
| 11     | 0   |                                                                                                                                          |
| 12     | 1   | <col=FF981F>Shifting Tombs</col> - 100% increased reputation.                                                                            |
| 13     | 0   |                                                                                                                                          |
| 14     | 1   | <col=FF981F>Soul Wars</col> - Double zeal.                                                                                               |
| 15     | 0   |                                                                                                                                          |
| 16     | 1   | <col=FF981F>Stealing Creation</col> - Double points.                                                                                     |
### Table 6
| row_id | int  |
| ------ | ---- |
| 0      | 6148 |
### Table 7
| row_id | int  |
| ------ | ---- |
| 0      | 6151 |
