## DBRow 1739 ([**51**](../dbtable/51.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 8   |
### Table 1
| row_id | string     |
| ------ | ---------- |
| 0      | Bank Chest |
### Table 3
| row_id | graphic |
| ------ | ------- |
| 0      | 1030    |
### Table 4
| row_id | dbrow |
| ------ | ----- |
| 0      | 1726  |
### Table 5
| row_id | int |
| ------ | --- |
| 0      | 500 |
### Table 6
| row_id | dbrow |
| ------ | ----- |
| 0      | 1730  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 500 |
### Table 10
| row_id | string                                   |
| ------ | ---------------------------------------- |
| 0      | Access to a bank chest at the Town Hall. |
### Table 11
| row_id | dbrow |
| ------ | ----- |
| 0      | 1754  |
