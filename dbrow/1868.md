## DBRow 1868 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                  |
| ------ | ------------------------------------------------------------------------------------------------------- |
| 0      | To start a fight, you need to click your opponent.<br>Left-click the training dummy to start attacking. |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 11
| row_id | int |
| ------ | --- |
| 0      | 1   |
