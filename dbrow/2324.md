## DBRow 2324 ([**29**](../dbtable/29.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 19  |
### Table 1
| row_id | int  |
| ------ | ---- |
| 0      | 1063 |
### Table 3
| row_id | int  |
| ------ | ---- |
| 0      | 1067 |
### Table 5
| row_id | string         |
| ------ | -------------- |
| 0      | Asciatops acta |
### Table 6
| row_id | string                      |
| ------ | --------------------------- |
| 0      | Ignore its toxic behaviour. |
### Table 7
| row_id | int   |
| ------ | ----- |
| 0      | 12906 |
### Table 8
| row_id | int  |
| ------ | ---- |
| 0      | 1290 |
### Table 9
| row_id | int  |
| ------ | ---- |
| 0      | 3228 |
### Table 10
| row_id | int  |
| ------ | ---- |
| 0      | 7098 |
### Table 11
| row_id | int  |
| ------ | ---- |
| 0      | 1060 |
### Table 12
| row_id | int  |
| ------ | ---- |
| 0      | 1061 |
### Table 13
| row_id | int  |
| ------ | ---- |
| 0      | 5000 |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 17
| row_id | int  |
| ------ | ---- |
| 0      | 1200 |
### Table 18
| row_id | int |
| ------ | --- |
| 0      | 800 |
### Table 19
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 24
| row_id | int    |
| ------ | ------ |
| 0      | 140000 |
### Table 25
| row_id | int    |
| ------ | ------ |
| 0      | 210000 |
### Table 26
| row_id | int    |
| ------ | ------ |
| 0      | 420000 |
### Table 27
| row_id | int    |
| ------ | ------ |
| 0      | 630000 |
### Table 28
| row_id | int  |
| ------ | ---- |
| 0      | 2520 |
### Table 29
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 48848 | 10  |
| 1      | 48850 | 10  |
### Table 42
| row_id | enum  | enum  |
| ------ | ----- | ----- |
| 0      | 15789 | 15790 |
### Table 46
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 47
| row_id | obj   |
| ------ | ----- |
| 0      | 48849 |
### Table 48
| row_id | int |
| ------ | --- |
| 0      | 6   |
