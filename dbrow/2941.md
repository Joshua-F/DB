## DBRow 2941 ([**91**](../dbtable/91.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 11  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 4   |
### Table 2
| row_id | string          |
| ------ | --------------- |
| 0      | Simon Templeton |
### Table 3
| row_id | string                                                            |
| ------ | ----------------------------------------------------------------- |
| 0      | Infamous pot-hunter. He's expensive, but has a lot of experience. |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 10447   |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 10477   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 6   |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 9   |
