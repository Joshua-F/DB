## DBRow 482 ([**4**](../dbtable/4.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 145 |
### Table 1
| row_id | string               |
| ------ | -------------------- |
| 0      | Brassican components |
### Table 2
| row_id | string                                       |
| ------ | -------------------------------------------- |
| 0      | Come on, are you even taking this seriously? |
### Table 4
| row_id | graphic |
| ------ | ------- |
| 0      | 26534   |
### Table 5
| row_id | vorbis |
| ------ | ------ |
| 0      | 12135  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 120 |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 4   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 38  |
### Table 10
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 22  | 36  | 9   |
| 1      | 23  | 45  | 8   |
| 2      | 32  | 49  | 8   |
### Table 11
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 22  | 36  | 9   |
| 1      | 23  | 45  | 8   |
| 2      | 32  | 49  | 8   |
### Table 12
| row_id | int | int | int |
| ------ | --- | --- | --- |
| 0      | 22  | 36  | 9   |
| 1      | 23  | 45  | 8   |
| 2      | 32  | 49  | 8   |
