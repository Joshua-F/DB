## DBRow 1600 ([**41**](../dbtable/41.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 1
| row_id | int | string               |
| ------ | --- | -------------------- |
| 0      | 1   | Apfel                |
| 1      | 2   | Brot                 |
| 2      | 3   | Cadantin             |
| 3      | 4   | Döner                |
| 4      | 5   | Erdbeeren            |
| 5      | 6   | Forelle              |
| 6      | 7   | Guam                 |
| 7      | 8   | Hai                  |
| 8      | 9   | Irit                 |
| 9      | 10  | Jangerbeeren         |
| 10     | 11  | Käse                 |
| 11     | 12  | Limpwurt             |
| 12     | 13  | Mehl                 |
| 13     | 14  | Nüsse                |
| 14     | 15  | Orange               |
| 15     | 16  | Pfirsich             |
| 16     | 17  | Quellwasser          |
| 17     | 18  | Rosmarin             |
| 18     | 19  | Seebarsch            |
| 19     | 20  | Tomate               |
| 20     | 21  | Ugthanki-Fleisch     |
| 21     | 22  | Vanillemilch         |
| 22     | 23  | Wassermelone         |
| 23     | 24  | Xenias Geheimtinktur |
| 24     | 25  | Yanillischer Hopfen  |
| 25     | 26  | Zwiebel              |
