## DBRow 1664 ([**40**](../dbtable/40.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 8   |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 218 |
### Table 3
| row_id | graphic | string                                                                                         | vorbis | int | boolean | boolean | boolean | boolean |
| ------ | ------- | ---------------------------------------------------------------------------------------------- | ------ | --- | ------- | ------- | ------- | ------- |
| 0      | 597     | The world guardian arrived just too late. Kerapac's plan had worked flawlessly.                | 43551  | 400 | 1       | 0       | 1       | 1       |
| 1      | 597     | With a triumphant shout he plunged the staff into the Needle to absorb its power.              | 43552  | 350 | 0       | 0       | 1       | 1       |
| 2      | 597     | Gail watched in horror as control of the Needle was torn from her and given to the dragonkin.  | 43553  | 400 | 0       | 1       | 1       | 1       |
| 3      | 598     | In an instant, she was Gail no more. Primrose stood, staring at the monster before her.        | 43555  | 500 | 1       | 0       | 1       | 1       |
| 4      | 598     | Kerapac roared in triumph, the power of the Needle now his to command.                         | 43558  | 350 | 0       | 1       | 1       | 1       |
| 5      | 599     | He took to the air, and the Needle vanished from sight at the whim of its new master.          | 43564  | 350 | 1       | 0       | 1       | 1       |
| 6      | 599     | He flew east to the coast and out across the sea, vanishing into a great storm.                | 43567  | 400 | 0       | 0       | 1       | 1       |
| 7      | 599     | And there he set to work on the next stage of his endeavour...                                 | 43603  | 350 | 0       | 1       | 1       | 1       |
