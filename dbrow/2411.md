## DBRow 2411 ([**75**](../dbtable/75.md))
### Table 0
| row_id | string        |
| ------ | ------------- |
| 0      | Join the Fun! |
### Table 1
| row_id | string                    |
| ------ | ------------------------- |
| 0      | Play the Latest Minigame! |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 29209   |
### Table 3
| row_id | graphic |
| ------ | ------- |
| 0      | 9643    |
| 1      | 9642    |
