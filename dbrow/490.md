## DBRow 490 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 16  |
### Table 1
| row_id | string     |
| ------ | ---------- |
| 0      | Antitheism |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 26343   |
### Table 4
| row_id | string                                           |
| ------ | ------------------------------------------------ |
| 0      | Denies access to protect prayers/deflect curses. |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 50  | 35  | 0   |
