## DBRow 2415 ([**75**](../dbtable/75.md))
### Table 0
| row_id | string               |
| ------ | -------------------- |
| 0      | Choose Your Destiny! |
### Table 1
| row_id | string                    |
| ------ | ------------------------- |
| 0      | Complete the Latest Path! |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 31692   |
### Table 3
| row_id | graphic |
| ------ | ------- |
| 0      | 9646    |
| 1      | 9648    |
| 2      | 9647    |
