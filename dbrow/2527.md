## DBRow 2527 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                         |
| ------ | -------------------------------------------------------------- |
| 0      | Left-click this button when you are ready to accept the quest. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 98304404  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 98304404  | -1  |
