## DBRow 527 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 6   |
### Table 1
| row_id | string |
| ------ | ------ |
| 0      | Lucky  |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 26366   |
### Table 4
| row_id | string                                                                                                                                                                                                                 |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | 0.5% chance per rank when hit that the damage dealt will be reduced to 1. Does not stack with the equivalent Warpriest effect.<br><col=0x7592A0>This perk has an increased chance to activate on level 20 items.</col> |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 50  | 35  | 0   |
| 1      | 2   | 80  | 65  | 0   |
| 2      | 3   | 130 | 120 | 0   |
| 3      | 4   | 170 | 160 | 0   |
| 4      | 5   | 210 | 195 | 0   |
| 5      | 6   | 250 | 205 | 1   |
