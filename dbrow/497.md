## DBRow 497 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 27  |
### Table 1
| row_id | string |
| ------ | ------ |
| 0      | Wise   |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 26341   |
### Table 4
| row_id | string                                                                       |
| ------ | ---------------------------------------------------------------------------- |
| 0      | While equipped, +1% per rank additional experience, up to 50,000 XP per day. |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 50  | 35  | 0   |
| 1      | 2   | 100 | 80  | 0   |
| 2      | 3   | 200 | 150 | 0   |
| 3      | 4   | 240 | 160 | 1   |
