## DBRow 1018 ([**28**](../dbtable/28.md))
### Table 0
| row_id | string              |
| ------ | ------------------- |
| 0      | Items Kept On Death |
### Table 1
| row_id | graphic |
| ------ | ------- |
| 0      | 24778   |
### Table 3
| row_id | string                                                                   |
| ------ | ------------------------------------------------------------------------ |
| 0      | View the items you are carrying and wearing which will be kept on death. |
### Table 4
| row_id | boolean | string              |
| ------ | ------- | ------------------- |
| 0      | 1       | Items Kept On Death |
