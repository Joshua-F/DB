## DBRow 3413 ([**86**](../dbtable/86.md))
### Table 0
| row_id | int   |
| ------ | ----- |
| 0      | 10026 |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 2
| row_id | string                        |
| ------ | ----------------------------- |
| 0      | Odd old man - excavation site |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 81  |
### Table 17
| row_id | dbrow |
| ------ | ----- |
| 0      | 2871  |
