## DBRow 2333 ([**29**](../dbtable/29.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 22  |
### Table 1
| row_id | int  |
| ------ | ---- |
| 0      | 1087 |
### Table 3
| row_id | int  |
| ------ | ---- |
| 0      | 1091 |
### Table 5
| row_id | string          |
| ------ | --------------- |
| 0      | Malletops palus |
### Table 6
| row_id | string                      |
| ------ | --------------------------- |
| 0      | Ignore its toxic behaviour. |
### Table 7
| row_id | int   |
| ------ | ----- |
| 0      | 15000 |
### Table 8
| row_id | int  |
| ------ | ---- |
| 0      | 1500 |
### Table 9
| row_id | int  |
| ------ | ---- |
| 0      | 3750 |
### Table 10
| row_id | int  |
| ------ | ---- |
| 0      | 8250 |
### Table 11
| row_id | int  |
| ------ | ---- |
| 0      | 1084 |
### Table 12
| row_id | int  |
| ------ | ---- |
| 0      | 1085 |
### Table 13
| row_id | int  |
| ------ | ---- |
| 0      | 5000 |
### Table 14
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 15
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 0   |
### Table 17
| row_id | int  |
| ------ | ---- |
| 0      | 1300 |
### Table 18
| row_id | int |
| ------ | --- |
| 0      | 800 |
### Table 19
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 24
| row_id | int    |
| ------ | ------ |
| 0      | 175000 |
### Table 25
| row_id | int    |
| ------ | ------ |
| 0      | 262500 |
### Table 26
| row_id | int    |
| ------ | ------ |
| 0      | 525000 |
### Table 27
| row_id | int    |
| ------ | ------ |
| 0      | 787500 |
### Table 28
| row_id | int  |
| ------ | ---- |
| 0      | 3276 |
### Table 29
| row_id | obj   | int |
| ------ | ----- | --- |
| 0      | 48872 | 10  |
| 1      | 48874 | 10  |
### Table 42
| row_id | enum  | enum  |
| ------ | ----- | ----- |
| 0      | 15789 | 15790 |
### Table 46
| row_id | boolean |
| ------ | ------- |
| 0      | 1       |
### Table 47
| row_id | obj   |
| ------ | ----- |
| 0      | 48873 |
### Table 48
| row_id | int |
| ------ | --- |
| 0      | 6   |
