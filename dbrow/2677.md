## DBRow 2677 ([**84**](../dbtable/84.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 119 |
### Table 2
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 4
| row_id | string       |
| ------ | ------------ |
| 0      | Armadylean I |
### Table 5
| row_id | boolean | boolean |
| ------ | ------- | ------- |
| 0      | 1       | 1       |
### Table 6
| row_id | string                                            |
| ------ | ------------------------------------------------- |
| 0      | King Oberon's moonshroom spores (relic component) |
### Table 7
| row_id | string                            |
| ------ | --------------------------------- |
| 0      | 50 Stormguard blueprint fragments |
### Table 9
| row_id | struct |
| ------ | ------ |
| 0      | 12288  |
### Table 11
| row_id | obj   |
| ------ | ----- |
| 0      | 49657 |
| 1      | 49681 |
| 2      | 49683 |
| 3      | 49631 |
| 4      | 49647 |
| 5      | 49649 |
| 6      | 49633 |
| 7      | 49637 |
| 8      | 49655 |
