## DBRow 2232 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                 |
| ------ | -------------------------------------- |
| 0      | Objects you can smelt are listed here. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 2424925   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 2424925   | -1  |
### Table 21
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 2424925   | -1  |
