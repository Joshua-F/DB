## DBRow 1237 ([**5**](../dbtable/5.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 138 |
### Table 1
| row_id | string                     |
| ------ | -------------------------- |
| 0      | Dungeoneering outfit piece |
### Table 2
| row_id | string                      |
| ------ | --------------------------- |
| 0      | Dungeoneering outfit pieces |
### Table 3
| row_id | string                                                                                 |
| ------ | -------------------------------------------------------------------------------------- |
| 0      | Pieces of the elite dungeoneering outfit that give bonuses when a full outfit is worn. |
### Table 5
| row_id | graphic |
| ------ | ------- |
| 0      | 839     |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 20  |
