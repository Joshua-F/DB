## DBRow 1857 ([**13**](../dbtable/13.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 10  |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 5   |
### Table 2
| row_id | string                   |
| ------ | ------------------------ |
| 0      | 5th August - 11th August |
### Table 3
| row_id | string           |
| ------ | ---------------- |
| 0      | Skilling Bonanza |
### Table 5
| row_id | int | string                                                                                                                                                                    |
| ------ | --- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | 0   | Gathering                                                                                                                                                                 |
| 1      | 1   | <col=FF981F>Divination</col> - Springs last twice as long and enriched wisps appear every 10 minutes.                                                                     |
| 2      | 1   | <col=FF981F>Fishing</col> - Spots move <col=FF981F>half as frequently</col> as before. Excludes fishing frenzy.                                                           |
| 3      | 1   | <col=FF981F>Hunter</col> - 10% chance of gaining one more creature than normal.                                                                                           |
| 4      | 1   | <col=FF981F>Mining</col> - Chance to get extra ore.                                                                                                                       |
| 5      | 1   | <col=FF981F>Uncharted Isles</col> - 25% chance not to deplete resources while harvesting.                                                                                 |
| 6      | 1   | <col=FF981F>Woodcutting</col> - Trees replenish twice as fast.                                                                                                            |
| 7      | 0   |                                                                                                                                                                           |
| 8      | 0   | Support                                                                                                                                                                   |
| 9      | 1   | <col=FF981F>Agility</col> - Higher chance of activating The Pit D&D.                                                                                                      |
| 10     | 1   | <col=FF981F>Thieving</col> - Bonus chance on jewelled statuettes whilst wearing the Ring of Wealth and doubled chance of sceptre and Black Ibis drops at Pyramid Plunder. |
| 11     | 0   |                                                                                                                                                                           |
| 12     | 0   | Artisan                                                                                                                                                                   |
| 13     | 1   | <col=FF981F>Construction</col> - Chance to retain a plank with Scroll of proficiency increased by 10%.                                                                    |
| 14     | 1   | <col=FF981F>Cooking</col> - Reduced chance to burn food.                                                                                                                  |
| 15     | 1   | <col=FF981F>Crafting</col> - 10% chance to produce an extra urn when adding runes to them.                                                                                |
| 16     | 1   | <col=FF981F>Fletching</col> - 25% increased Fletching experience.                                                                                                         |
| 17     | 1   | <col=FF981F>Herblore</col> - Extra 2.5% chance to save a secondary ingredient when mixing potions.                                                                        |
| 18     | 1   | <col=FF981F>Smithing</col> - Double respect and luminite injector effect in the Artisans' Workshop.                                                                       |
| 19     | 1   | <col=FF981F>Runecrafting</col> - Increased node spawns and double runes at Runespan.                                                                                      |
### Table 6
| row_id | int  |
| ------ | ---- |
| 0      | 6368 |
### Table 7
| row_id | int  |
| ------ | ---- |
| 0      | 6374 |
