## DBRow 1893 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                          |
| ------ | ------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Wield the wand.<br>When you attack with a magic weapon, you will find that it automatically casts the last spell you last used. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 96534535  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 3   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 96534535  | 0   |
