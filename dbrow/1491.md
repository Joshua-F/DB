## DBRow 1491 ([**37**](../dbtable/37.md))
### Table 0
| row_id | string           | enum  |
| ------ | ---------------- | ----- |
| 0      | Armour           | 15029 |
| 1      | Weapons          | 15027 |
| 2      | Off-Hand Weapons | 15028 |
| 3      | Ranged           | 15031 |
| 4      | Misc             | 15030 |
