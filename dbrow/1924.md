## DBRow 1924 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                   |
| ------ | ------------------------------------------------------------------------------------------------------------------------ |
| 0      | Choose a combat style to use for this section of the course. When you have decided, tap the armour stand for that style. |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 11
| row_id | int |
| ------ | --- |
| 0      | 3   |
