## DBRow 1454 ([**34**](../dbtable/34.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 60  |
### Table 1
| row_id | string      |
| ------ | ----------- |
| 0      | Runite rock |
### Table 3
| row_id | obj | int | int |
| ------ | --- | --- | --- |
| 0      | 451 | 50  | 100 |
