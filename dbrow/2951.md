## DBRow 2951 ([**89**](../dbtable/89.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 2
| row_id | string                                                                                                                                                               |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | This multi-coloured coat is very vibrant and distinctive. It perhaps suggests a connection between the aviansie and colourful mating displays by many types of bird. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 81  |
### Table 4
| row_id | obj   |
| ------ | ----- |
| 0      | 49632 |
### Table 5
| row_id | obj   |
| ------ | ----- |
| 0      | 49633 |
