## DBRow 997 ([**22**](../dbtable/22.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 13  |
### Table 1
| row_id | string                    |
| ------ | ------------------------- |
| 0      | High capacity plank maker |
### Table 2
| row_id | string                 |
| ------ | ---------------------- |
| 0      | Saws logs into planks. |
### Table 3
| row_id | int |
| ------ | --- |
| 0      | 117 |
### Table 4
| row_id | int |
| ------ | --- |
| 0      | 129 |
### Table 5
| row_id | dbrow |
| ------ | ----- |
| 0      | 996   |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 7
| row_id | dbrow | int |
| ------ | ----- | --- |
| 0      | 450   | 30  |
| 1      | 429   | 300 |
| 2      | 438   | 20  |
| 3      | 439   | 20  |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 6   |
### Table 10
| row_id | int  |
| ------ | ---- |
| 0      | 7500 |
### Table 12
| row_id | enum  |
| ------ | ----- |
| 0      | 13179 |
### Table 16
| row_id | int |
| ------ | --- |
| 0      | 40  |
### Table 20
| row_id | int |
| ------ | --- |
| 0      | 65  |
### Table 21
| row_id | int |
| ------ | --- |
| 0      | 600 |
### Table 22
| row_id | int  |
| ------ | ---- |
| 0      | 7100 |
