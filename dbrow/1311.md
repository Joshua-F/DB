## DBRow 1311 ([**31**](../dbtable/31.md))
### Table 0
| row_id | string            |
| ------ | ----------------- |
| 0      | Player Owned Home |
### Table 1
| row_id | string |
| ------ | ------ |
| 0      |        |
### Table 2
| row_id | struct |
| ------ | ------ |
| 0      | 41743  |
| 1      | 41744  |
| 2      | 41745  |
| 3      | 41815  |
| 4      | 41542  |
| 5      | 41543  |
| 6      | 41544  |
