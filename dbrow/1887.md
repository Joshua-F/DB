## DBRow 1887 ([**72**](../dbtable/72.md))
### Table 3
| row_id | string                                                                                                                              |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Wield the shortbow and arrows in the same way you equipped the scimitar: open your backpack and click the shortbow then the arrows. |
### Table 5
| row_id | component |
| ------ | --------- |
| 0      | 96534535  |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | -1  |
### Table 7
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 9
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 12
| row_id | int |
| ------ | --- |
| 0      | 1   |
### Table 20
| row_id | component | int |
| ------ | --------- | --- |
| 0      | 96534535  | 0   |
