## DBRow 3315 ([**86**](../dbtable/86.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 407 |
### Table 1
| row_id | int |
| ------ | --- |
| 0      | 2   |
### Table 2
| row_id | string                                                                 |
| ------ | ---------------------------------------------------------------------- |
| 0      | Stormguard Citadel - Research & Development south-west excavation site |
### Table 6
| row_id | int |
| ------ | --- |
| 0      | 98  |
### Table 17
| row_id | dbrow |
| ------ | ----- |
| 0      | 3319  |
| 1      | 2852  |
