## DBRow 2405 ([**75**](../dbtable/75.md))
### Table 0
| row_id | string               |
| ------ | -------------------- |
| 0      | Fancy a Quick Quest? |
### Table 1
| row_id | string                         |
| ------ | ------------------------------ |
| 0      | Complete the Latest Miniquest! |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 30103   |
### Table 3
| row_id | graphic |
| ------ | ------- |
| 0      | 9650    |
| 1      | 9649    |
