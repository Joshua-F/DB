## DBRow 817 ([**8**](../dbtable/8.md))
### Table 0
| row_id | int |
| ------ | --- |
| 0      | 68  |
### Table 1
| row_id | string |
| ------ | ------ |
| 0      | Rapid  |
### Table 2
| row_id | graphic |
| ------ | ------- |
| 0      | 28224   |
### Table 4
| row_id | string                                                                                                                                                                                                                                                               |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Has a 5% chance per rank to carry out work at a faster pace. However, while smithing has a 5% chance per rank to double your progress while your heat is lost twice as fast.<br><col=0x7592A0>This perk has an increased chance to activate on level 20 items.</col> |
### Table 7
| row_id | int | int | int | int |
| ------ | --- | --- | --- | --- |
| 0      | 1   | 55  | 40  | 0   |
| 1      | 2   | 115 | 90  | 0   |
| 2      | 3   | 220 | 175 | 0   |
| 3      | 4   | 260 | 185 | 1   |
